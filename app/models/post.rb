class Post < ActiveRecord::Base
  after_create :create_post_id
  before_save :update_post_id
  before_save :save_version

  COLUMNS_THAT_TRIGGER_NEW_VERSION = ["title", "description"]

  default_scope { order('post_id, published_at desc') } 
  scope :last_published, -> { joins('join (select distinct on (inner_posts.post_id) inner_posts.id from posts as inner_posts order by inner_posts.post_id, inner_posts.published_at desc nulls last) as published_posts on published_posts.id = posts.id')}


  has_many :versions, class_name: "Post", primary_key: "post_id", foreign_key: 'post_id'

  #has_many :post_versions, -> { where post_id: 22 }, class_name: 'Post'
  has_one :last_version, -> { last_published }, class_name: 'Post'

  attr_accessor :is_clone

  def update_post_id
    self.post_id ||= self.id
  end

  def create_post_id
    return if post_id
    update(post_id: id)
  end

  # def versions
  #   self.class.where(post_id: self.post_id).where.not(id: self.id)
  # end

  def save_version
    return true if (changed_attributes.keys & COLUMNS_THAT_TRIGGER_NEW_VERSION).size == 0
    
    return if is_clone? #avoid nasty recursive loops
    return unless published? #don't create new version, unless the version itself is published
    self.version_clone
    self.restore_attributes
  end

  def published?
    published_at.present?
  end

  def publish
    published_at = Time.now
    self
  end

  def unpublish
    published_at = nil
    self
  end

  def publish!
    update(published_at: Time.now)
  end

  def unpublish!
    update(published_at: nil)
  end

  def is_clone?
    is_clone == true
  end

  def version_clone
    version_clone = dup
    version_clone.is_clone = true
    version_clone.published_at = nil
    version_clone.save
    version_clone
  end

end
